#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_project_data_ext.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IUserTaskProjectDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IUserTaskProjectDataExtention IDataExtention)

	DATA_EXTENTION_BASE_DEFINITIONS(IUserTaskProjectDataExtention, IUserTaskDataExtention, {"isProject"})

public:
	DataExtention(QObject* parent, ReferenceInstancePtr<IUserTaskDataExtention> extention) :
		QObject(parent),
		m_extention(extention)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

public:
	bool isProject() override
	{
		return false;
	}

	QString name() override
	{
		return m_extention->name();
	}

	bool isDone() override
	{
		return m_extention->isDone();
	}

private:
	ReferenceInstancePtr<IUserTaskDataExtention> m_extention;
};
